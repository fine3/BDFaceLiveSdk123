package com.baidu.idl.facesdk;

public class FaceVerifyData {
    public int[] mRegImg;
    public int[] mRegLdmk;
    public byte[] mRegDigest;
    public int rows;
    public int cols;
    public int nPoints;

    FaceVerifyData() {
    }

    FaceVerifyData(int[] img, int[] ldmk, byte[] digeststr, int _rows, int _cols, int _nPoints) {
        this.mRegImg = img;
        this.mRegLdmk = ldmk;
        this.mRegDigest = digeststr;
        this.rows = _rows;
        this.cols = _cols;
        this.nPoints = _nPoints;
    }
}
