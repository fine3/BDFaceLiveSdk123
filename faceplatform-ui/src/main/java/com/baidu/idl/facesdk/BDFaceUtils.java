package com.baidu.idl.facesdk;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;

import java.io.InputStream;

public class BDFaceUtils {
    public BDFaceUtils() {
    }

    public static boolean hasModel(Context context, String modelPath) {
        if (context != null && !TextUtils.isEmpty(modelPath)) {
            try {
                AssetManager assetManager = context.getAssets();
                InputStream stream = assetManager.open(modelPath);
                if (stream != null) {
                    return true;
                }
            } catch (Exception var4) {
                var4.printStackTrace();
            }
        }
        return false;
    }
}
