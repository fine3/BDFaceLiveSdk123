package com.baidu.idl.facesdk;

public class FaceSleepnessInfo {
    public int[] landmarks;
    public float[] headPose;
    public float[] occlusions;
    public float bluriness;
    public float illum;
    public float left_eye_open_prob;
    public float left_eye_close_prob;
    public float right_eye_open_prob;
    public float right_eye_close_prob;
    public float mouth_open_prob;
    public float mouth_close_prob;

    public FaceSleepnessInfo(float left_eye_open_prob, float left_eye_close_prob, float right_eye_open_prob, float right_eye_close_prob, float mouth_open_prob, float mouth_close_prob, float bluriness, float illum, int[] landmarks, float[] headPose, float[] occlusions) {
        this.left_eye_open_prob = left_eye_open_prob;
        this.left_eye_close_prob = left_eye_close_prob;
        this.right_eye_open_prob = right_eye_open_prob;
        this.right_eye_close_prob = right_eye_close_prob;
        this.mouth_open_prob = mouth_open_prob;
        this.mouth_close_prob = mouth_close_prob;
        this.bluriness = bluriness;
        this.illum = illum;
        this.landmarks = landmarks;
        this.headPose = headPose;
        this.occlusions = occlusions;
    }
}
