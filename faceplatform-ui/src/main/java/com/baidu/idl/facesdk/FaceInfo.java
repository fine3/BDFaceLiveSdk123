package com.baidu.idl.facesdk;

public class FaceInfo {
    public int mWidth;
    public int mAngle;
    public int mCenter_y;
    public int mCenter_x;
    public float mConf;
    public int[] landmarks;
    public int face_id;
    public float[] headPose;
    public int[] is_live;

    public FaceInfo(int width, int angle, int y, int x, float conf) {
        this.mWidth = width;
        this.mAngle = angle;
        this.mCenter_y = y;
        this.mCenter_x = x;
        this.mConf = conf;
        this.landmarks = null;
        this.face_id = 0;
    }

    public FaceInfo(int width, int angle, int y, int x, float conf, int track_id, int[] ldmks) {
        this.mWidth = width;
        this.mAngle = angle;
        this.mCenter_y = y;
        this.mCenter_x = x;
        this.mConf = conf;
        this.landmarks = ldmks;
        this.face_id = track_id;
    }

    public FaceInfo(int width, int angle, int y, int x, float conf, int track_id, int[] ldmks, float[] pose, int[] livestatus) {
        this.mWidth = width;
        this.mAngle = angle;
        this.mCenter_y = y;
        this.mCenter_x = x;
        this.mConf = conf;
        this.landmarks = ldmks;
        this.face_id = track_id;
        this.headPose = pose;
        this.is_live = livestatus;
    }

    public void getRectPoints(int[] pts) {
        double degree_rad = (double) this.mAngle * 3.14159D / 180.0D;
        double cos_degree = Math.cos(degree_rad);
        double sin_degree = Math.sin(degree_rad);
        int center_x = (int) ((double) this.mCenter_x + cos_degree * (double) this.mWidth / 2.0D - sin_degree * (double) this.mWidth / 2.0D);
        int center_y = (int) ((double) this.mCenter_y + sin_degree * (double) this.mWidth / 2.0D + cos_degree * (double) this.mWidth / 2.0D);
        double _angle = (double) this.mAngle * 3.14159D / 180.0D;
        double b = Math.cos(_angle) * 0.5D;
        double a = Math.sin(_angle) * 0.5D;
        if (pts == null || pts.length == 0) {
            pts = new int[8];
        }

        pts[0] = (int) ((double) center_x - a * (double) this.mWidth - b * (double) this.mWidth);
        pts[1] = (int) ((double) center_y + b * (double) this.mWidth - a * (double) this.mWidth);
        pts[2] = (int) ((double) center_x + a * (double) this.mWidth - b * (double) this.mWidth);
        pts[3] = (int) ((double) center_y - b * (double) this.mWidth - a * (double) this.mWidth);
        pts[4] = 2 * center_x - pts[0];
        pts[5] = 2 * center_y - pts[1];
        pts[6] = 2 * center_x - pts[2];
        pts[7] = 2 * center_y - pts[3];
    }

    public boolean is_live() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[0];
        } else {
            return false;
        }
    }

    public boolean is_live_mouth() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[3];
        } else {
            return false;
        }
    }

    public boolean is_live_head_turn_left() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[5];
        } else {
            return false;
        }
    }

    public boolean is_live_head_turn_right() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[6];
        } else {
            return false;
        }
    }

    public boolean is_live_head_up() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[8];
        } else {
            return false;
        }
    }

    public boolean is_live_head_down() {
        if (this.is_live != null && this.is_live.length == 11) {
            return 1 == this.is_live[9];
        } else {
            return false;
        }
    }

    public int get_leftEyeState() {
        return this.is_live != null && this.is_live.length == 11 ? this.is_live[1] : 0;
    }

    public int get_rightEyeState() {
        return this.is_live != null && this.is_live.length == 11 ? this.is_live[2] : 0;
    }

    public int get_mouthState() {
        return this.is_live != null && this.is_live.length == 11 ? this.is_live[4] : 0;
    }
}
