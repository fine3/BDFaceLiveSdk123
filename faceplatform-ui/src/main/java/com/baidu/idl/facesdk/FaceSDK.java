package com.baidu.idl.facesdk;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

public class FaceSDK {
    private static final int mAuthorityStatus = 0;

    public FaceSDK() {
    }

    public static synchronized void initLicense() {
        try {
            System.loadLibrary("baidu_license");
            System.loadLibrary("FaceSDK");
            int numOfCpuCore = getNumCores();
            int numOfThreads = numOfCpuCore > 1 ? numOfCpuCore / 2 : 1;
            setNumberOfThreads(numOfThreads);
        } catch (Exception ignored) {
        }
    }

    public static int getAuthorityStatus() {
        return mAuthorityStatus;
    }

    private static int getNumCores() {
        try {
            File dir = new File("/sys/devices/system/cpu/");
            class CpuFilter implements FileFilter {
                CpuFilter() {
                }

                public boolean accept(File pathname) {
                    return Pattern.matches("cpu[0-9]+", pathname.getName());
                }
            }
            File[] files = dir.listFiles(new CpuFilter());
            return files.length;
        } catch (Exception var2) {
            return 1;
        }
    }

    public static boolean checkParameter(Object img, int rows, int cols) {
        return img != null && rows > 0 && cols > 0;
    }

    public static native int setPerfLogFlag(int var0);

    public static native int setValueLogFlag(int var0);

    public static native int setNumberOfThreads(int var0);

    public static native int getARGBFromYUVimg(byte[] var0, int[] var1, int var2, int var3, int var4, int var5);

    public enum QualityModelType {
        QUALITY_NOT_USE,
        QUALITY_BLUR,
        QUALITY_OCCLUSION;

        QualityModelType() {
        }
    }

    public enum AlignMethodType {
        SDM,
        CDNN,
        SDM_7PTS,
        SDM_15PTS;

        AlignMethodType() {
        }
    }

    public enum DetectMethodType {
        BOOST,
        CNN,
        NIR;

        DetectMethodType() {
        }
    }

    public enum ImgType {
        ARGB;

        ImgType() {
        }
    }
}
