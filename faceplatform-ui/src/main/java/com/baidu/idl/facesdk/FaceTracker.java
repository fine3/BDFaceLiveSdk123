package com.baidu.idl.facesdk;

import android.content.Context;
import android.content.res.AssetManager;

import com.baidu.idl.facesdk.FaceSDK.AlignMethodType;
import com.baidu.idl.facesdk.FaceSDK.DetectMethodType;
import com.baidu.idl.facesdk.FaceSDK.ImgType;
import com.baidu.idl.facesdk.FaceSDK.QualityModelType;

import java.util.HashMap;
import java.util.Map;

public class FaceTracker {
    private final Map<String, Boolean> abilities = new HashMap();
    private final long index = 1L;
    private Context context;

    public FaceTracker(Context context) {
        if (context != null) {
            this.create();
            if (BDFaceUtils.hasModel(context, "align_model.binary")) {
                this.AlignModelInit(context.getAssets(), "align_model.binary", "", AlignMethodType.CDNN.ordinal());
                this.abilities.put("align_model.binary", true);
            }

            if (BDFaceUtils.hasModel(context, "facedetect.binary")) {
                this.DetectModelInit(context.getAssets(), "facedetect.binary", DetectMethodType.CNN.ordinal());
                this.abilities.put("facedetect.binary", true);
            }

            if (BDFaceUtils.hasModel(context, "score.binary")) {
                this.ScoreModelInit(context.getAssets(), "score.binary", "", DetectMethodType.CNN.ordinal());
                this.abilities.put("score.binary", true);
            }

            if (BDFaceUtils.hasModel(context, "blur.binary")) {
                this.imgQualityModelInit(context.getAssets(), "blur.binary", "", QualityModelType.QUALITY_BLUR.ordinal());
                this.abilities.put("blur.binary", true);
            }

            if (BDFaceUtils.hasModel(context, "occlu.binary")) {
                this.imgQualityModelInit(context.getAssets(), "occlu.binary", "", QualityModelType.QUALITY_OCCLUSION.ordinal());
                this.abilities.put("occlu.binary", true);
            }
        }

    }

    public void track(int[] img, int rows, int cols, int imgType, int maxTrackObjNum) {
        if (FaceSDK.checkParameter(img, rows, cols)) {
            this.tracking(img, rows, cols, imgType, maxTrackObjNum);
        }
    }

    public FaceTracker.ErrCode faceVerification(int[] img, int rows, int cols, ImgType imgType, FaceTracker.ActionType action) {
        if (!FaceSDK.checkParameter(img, rows, cols)) {
            return FaceTracker.ErrCode.UNKNOW_TYPE;
        } else {
            int err_code = this.prepare_data_for_verify(img, rows, cols, imgType.ordinal(), action.ordinal());
            if (err_code == FaceTracker.ErrCode.OK.ordinal()) {
                return FaceTracker.ErrCode.OK;
            } else {
                return err_code > 0 ? FaceTracker.ErrCode.values()[err_code] : FaceTracker.ErrCode.UNKNOW_TYPE;
            }
        }
    }

    public FaceTracker.ErrCode maxFaceVerification(int[] img, int rows, int cols, ImgType imgType, FaceTracker.ActionType action) {
        if (!FaceSDK.checkParameter(img, rows, cols)) {
            return FaceTracker.ErrCode.UNKNOW_TYPE;
        } else {
            int err_code = this.prepare_max_face_data_for_verify(img, rows, cols, imgType.ordinal(), action.ordinal());
            if (err_code == FaceTracker.ErrCode.OK.ordinal()) {
                return FaceTracker.ErrCode.OK;
            } else {
                return err_code > 0 ? FaceTracker.ErrCode.values()[err_code] : FaceTracker.ErrCode.UNKNOW_TYPE;
            }
        }
    }

    public native FaceInfo[] get_TrackedFaceInfo();

    public native FaceSleepnessInfo[] get_sleepnessInfo();

    private native int create();

    private native int AlignModelInit(AssetManager var1, String var2, String var3, int var4);

    private native int DetectModelInit(AssetManager var1, String var2, int var3);

    private native int ScoreModelInit(AssetManager var1, String var2, String var3, int var4);

    private native int ParsingModelInit(AssetManager var1, String var2, String var3, int var4);

    private native int imgQualityModelInit(AssetManager var1, String var2, String var3, int var4);

    private native void tracking(int[] var1, int var2, int var3, int var4, int var5);

    public native int set_notFace_thr(float var1);

    public native int set_min_face_size(int var1);

    public native int set_illum_thr(float var1);

    public native int set_blur_thr(float var1);

    public native int set_occlu_thr(float var1);

    /**
     * @deprecated
     */
    @Deprecated
    public native int set_detection_frame_interval(int var1);

    public native int set_AlignMethodType(int var1);

    public native int set_isFineAlign(boolean var1);

    public native int set_isVerifyLive(boolean var1);

    public native int set_isCheckQuality(boolean var1);

    /**
     * @deprecated
     */
    @Deprecated
    public native int set_intervalTime(long var1);

    public native int set_track_by_detection_interval(int var1);

    public native int set_detect_in_video_interval(int var1);

    public native int set_eulur_angle_thr(int var1, int var2, int var3);

    public native int set_cropFaceSize(int var1);

    public native int set_max_reg_img_num(int var1);

    public native int clearTrackedFaces();

    public native int re_collect_reg_imgs();

    public native FaceVerifyData[] get_FaceVerifyData(int var1);

    public native int prepare_data_for_verify(int[] var1, int var2, int var3, int var4, int var5);

    public native int prepare_max_face_data_for_verify(int[] var1, int var2, int var3, int var4, int var5);

    public native int set_DetectMethodType(int var1);

    public native int set_cropFaceEnlargeRatio(float var1);

    public native int setAppType(int var1);

    public enum ActionType {
        DELETE,
        REGIST,
        VERIFY,
        RECOGNIZE;

        ActionType() {
        }
    }

    public enum ErrCode {
        OK,
        PITCH_OUT_OF_DOWN_MAX_RANGE,
        PITCH_OUT_OF_UP_MAX_RANGE,
        YAW_OUT_OF_LEFT_MAX_RANGE,
        YAW_OUT_OF_RIGHT_MAX_RANGE,
        POOR_ILLUMINATION,
        NO_FACE_DETECTED,
        DATA_NOT_READY,
        DATA_HIT_ONE,
        DATA_HIT_LAST,
        IMG_BLURED,
        OCCLUSION_LEFT_EYE,
        OCCLUSION_RIGHT_EYE,
        OCCLUSION_NOSE,
        OCCLUSION_MOUTH,
        OCCLUSION_LEFT_CONTOUR,
        OCCLUSION_RIGHT_CONTOUR,
        OCCLUSION_CHIN_CONTOUR,
        FACE_NOT_COMPLETE,
        UNKNOW_TYPE;

        ErrCode() {
        }
    }
}
